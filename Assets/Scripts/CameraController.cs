﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform player; private Vector3 cameraPos;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        cameraPos = player.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.localPosition.x < 10.0f && player.localPosition.x > -10.0f)
        {
            transform.position = player.position - cameraPos;
        }
    }

}
