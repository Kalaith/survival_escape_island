﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Player player;
    public static PlayerController playerc;

    // Start is called before the first frame update
    void Start()
    {
        player = new Player("Bob");
    }

    private void Awake()
    {
        playerc = this;
    }

    // Update is called once per frame
    void Update()
    {
        player.Update();
        Debug.Log("Health: " + player.health);
        Debug.Log("Hunger: " + player.hunger);
        Debug.Log("Thirst: " + player.thirst);
    }
}
