﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// base player class for holding data
[SerializeField]
public class Player
{
    public static double DEFAULT_START_HEALTH = 100;
    public static double DEFAULT_START_HUNGER = 100;
    public static double DEFAULT_START_THIRST = 100;
    public static double TICK_LENGTH = 5;

    public string name;
    public double health;
    public double hunger;
    public double thirst;

    public double tick;

    public Player(string name)
    {
        this.name = name;
        health = DEFAULT_START_HEALTH;
        hunger = DEFAULT_START_HUNGER;
        thirst = DEFAULT_START_THIRST;

    }

    public void Eat(double food, double life = 0)
    {
        hunger += food;
        health += life;
    }

    public void Drink(double liquid, double life = 0)
    {
        thirst += liquid;
        health += life;
    }

    public void Update()
    {
        tick += Time.deltaTime;
        if(tick > TICK_LENGTH)
        {
            tick = 0;
            hunger--;
            thirst--;

            if(hunger <= 0)
            {
                hunger = 0;
                health--;
            }

            if(thirst <= 0)
            {
                thirst--;
                health--;
            }
        }
    }
}
